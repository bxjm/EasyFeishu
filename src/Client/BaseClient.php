<?php

namespace bxjm\EasyFeishu\Client;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use bxjm\EasyFeishu\Client\Log\LoggerManager;

class BaseClient extends Client
{
    public array $easyFeiShuConfig = [];

    public function __construct(array $config = [])
    {
        Client::__construct($config);
    }

    public function post($uri, array $options = []): ResponseInterface
    {
        return $this->clientRequest('POST', $uri, $options);
    }

    public function get($uri, array $options = []): ResponseInterface
    {
        return $this->clientRequest('GET', $uri, $options);
    }

    public function clientRequest(string $method, $uri = '', array $options = []): ResponseInterface
    {
        //临时设置，中国时区
        date_default_timezone_set('PRC');

        //将 multipart 类型中的 文件流释放
        $cloneOptions = $options;
        unset($cloneOptions['headers'], $cloneOptions['http_errors']);
        if (isset($options['multipart'])) {
            foreach ($options['multipart'] as $multipartKey => $multipartValue) {
                if (!empty($multipartValue['contents']) && is_resource($multipartValue['contents'])) {
                    unset($cloneOptions['multipart'][$multipartKey]);
                }
            }
        }

        //request
        $response = $this->request($method, $uri, $options);

        if (!empty($this->easyFeiShuConfig)) {
            $requestHeaders = $options['headers'] ?? [];

            //数据库日志
            if (!empty($this->easyFeiShuConfig['mysql_log'])) {
                LoggerManager::createMysqlLog($uri, $requestHeaders, $cloneOptions, $method, $this->easyFeiShuConfig, $response);
            }

            //文件日志
            if (!empty($this->easyFeiShuConfig['log'])) {
                $logId = strtoupper(md5(uniqid(rand(), true)));
                list($usec, $sec) = explode(" ", microtime());
                $response->getBody()->rewind();
                $responseBody = $response->getBody()->getContents() ?? '';
                $response->getBody()->rewind();

                //判断结果是否是json格式
                if (empty(json_decode($responseBody, true))) {
                    $responseBody = json_encode([$responseBody]);
                } else {
                    $responseBody = json_decode($responseBody, true);
                }

                LoggerManager::createDailyDriver($this->easyFeiShuConfig['log']['name'], $this->easyFeiShuConfig['log']['path'])
                    ->info('apiLog', [
                    'log_id' => $logId,
                    'request_time' => '时间：' . date('Y-m-d H:i:s') . "毫秒时间戳" . ((float)$usec + (float)$sec),
                    'request_url' => $uri,
                    'request_header' => $requestHeaders,
                    'request_body' => $cloneOptions,
                    'response_status' => $response->getStatusCode(),
                    'response_header' => $response->getHeaders(),
                    'response_body' => $responseBody,
                ]);
            }
        }

        return $response;
    }

    public function easyFeiShuConfig($easyFeiShuConfig = [])
    {
        $this->easyFeiShuConfig = $easyFeiShuConfig;
        return $this;
    }
}
