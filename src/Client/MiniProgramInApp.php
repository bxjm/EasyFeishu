<?php

namespace bxjm\EasyFeishu\Client;

// https://open.feishu.cn/document/uYjL24iN/ukTO4UjL5kDO14SO5gTN
// 身份验证(免登) - 应用免登概述 - 小程序免登
class MiniProgramInApp
{
    use \bxjm\EasyFeishu\Traits\HasHttpMgr;

    public function __construct($server)
    {
        $this->tokenMgr = $server->tokenMgr;
        $this->httpClient = new \GuzzleHttp\Client();
    }

    public function getUserFromCode($code)
    {
        return $this->code2session($code, 'string');
    }

    // https://open.feishu.cn/document/uYjL24iN/ukjM04SOyQjL5IDN
    // code2session
    private function code2session($code, $format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/mina/v2/tokenLoginValidate';
        $headers = $this->defautHeaders + [
            'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
        ];
        $post_data = [
            'code' => $code
        ];
        try {
            $response = $this->httpClient->post(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'json' => $post_data
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = ['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()];
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }
}
