<?php

namespace bxjm\EasyFeishu\Client;

// https://open.feishu.cn/document/uYjL24iN/ukTO4UjL5kDO14SO5gTN
// 身份验证(免登) - 应用免登概述 - 客户端内网页免登
use bxjm\EasyFeishu\Client\Log\LoggerManager;

class H5InApp
{
    // TBD
    use \bxjm\EasyFeishu\Traits\HasHttpMgr;

    public function __construct($server)
    {
        $this->tokenMgr = $server->tokenMgr;
        $this->httpClient = new \GuzzleHttp\Client();
    }

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/authen-v1/authen/access_token
    // 获取登录用户身份
    public function getUserInfoByAuthCode($code, $format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/authen/v1/access_token';
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        $post_data = [
            'grant_type' => 'authorization_code',
            'code' => $code
        ];
        try {
            $response = $this->httpClient->post(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'json' => $post_data
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = ['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()];
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }
}
