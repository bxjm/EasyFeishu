<?php

namespace bxjm\EasyFeishu\Traits;

use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\MessageFormatter;
use GuzzleHttp\MessageFormatterInterface;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use bxjm\EasyFeishu\Client\Log\LoggerManager;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use bxjm\EasyFeishu\Client\BaseClient;

trait HasHttpMgr
{
    private $feishu_oapi_domain_1 = 'https://open.feishu.cn/open-apis';
    private $feishu_oapi_domain_2 = 'https://www.feishu.cn';
    private $defautHeaders = [
        'Accept' => 'application/json',
        'Content-Type' => 'application/json; charset=utf-8',
    ];
    private $defautGuzzleOption = [
        'http_errors' => false,
    ];

    private $tokenMgr;
    private $httpClient;
    private $config;

    public function __construct($tokenMgr, $config, ?ResponseInterface $response = null)
    {
        $this->tokenMgr = $tokenMgr;
        $this->config = $config;
        $handlerStack = HandlerStack::create();
        //clientLog
        // if (isset($this->config['log'])) {
        //     $logPath = $this->config['log']['path'] . '/' . $this->config['log']['name'] . '-' . date('Y-m-d') . '.log';
        //     $logger = new Logger('mylogger');
        //     $logger->pushHandler(new StreamHandler($logPath), Logger::DEBUG);
        //     $logId = strtoupper(md5(uniqid(rand(), true)));
        //     $logInfo = "logId:" . $logId . "\n" .
        //         "request_time:{ts}\n" .
        //         "request_url:{uri}\n" .
        //         "request_header:{req_headers}\n" .
        //         "request_body:{req_body}\n" .
        //         "response_status:{code}\n" .
        //         "response_header:{res_headers}\n" .
        //         "response_body:{res_body}\n" .
        //         "error:{error}";
        //     $handlerStack->push(Middleware::log($logger, new MessageFormatter($logInfo)));
        // }
        $this->httpClient = (new BaseClient(['handler' => $handlerStack]))->easyFeiShuConfig($config);
    }
}
