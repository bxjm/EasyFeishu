<?php

namespace bxjm\EasyFeishu\Traits;

// phpcs:disable Generic.Files.LineLength
trait HasEventHandler
{
    protected $encryptKey;

    // https://open.feishu.cn/document/ukTMukTMukTM/uUTNz4SN1MjL1UzM
    // 事件订阅概述
    // $request : Symfony\Component\HttpFoundation\Request
    public function serve($request)
    {
        // if (!is_a($request, 'Symfony\Component\HttpFoundation\Request')) {
        //     echo '$request must be a instance of [Symfony\Component\HttpFoundation\Request]';
        //     return null;
        // }
        if ($request->input('encrypt')) {
            $request_array = $this->decrypt($request);
        } else {
            $request_array = $request->all();
        }

        // https://open.feishu.cn/document/ukTMukTMukTM/uYDNxYjL2QTM24iN0EjN/event-subscription-configure-/request-url-configuration-case
        // 请求网址配置示例
        if (array_key_exists('challenge', $request_array)) {
            // response
            return array_intersect_key($request_array, ['challenge' => '']);
        } else {
            // https://open.feishu.cn/document/ukTMukTMukTM/uYDNxYjL2QTM24iN0EjN/event-list
            // 事件列表
            return $request_array;
        }
    }

    /*
       For Test
           $encryptKey = 'mRhyJKSrjURqYR8rYKjg2cwzVvx2lxtJ';
           $encryptData = "dJP95f2TTJg7UVWxXR3nnUVUhURdKeUxv6nDgKOEdi51k3BYxtrfccdolGWDVb+MwRdKsfU/K6Hq3F7j0a7Q9ECFMZV3yxMAvDEvNOgatCVYYgR/zmhe6oR4TjYnJ9hHZmJ4k1LAtQZFhZj41jOwf5BAh3WjYr7tmCl9E4rMdG4gynveJOV5LRGN5qJa5MYD";
           $encryptData = "FOrefpkMVPIoyVKFgOT4+BmpuHTtsmImDRj4E32O6vVZX1+sPVoeCGXJKNK1CKcG2eHqU7w2t85fgLYxniQ+hVLn8bxRFoyFuvjYxga5QORtktsav8aolxSByZApaf6/1PfM6kxhpm1BmRA6tF8/L2iZOydOOxHvIVrM/Lq6QQBLS0wr7cqTquVHjQY4GC0ntf5wb+QgiafON9Y2FtP9Mbm5S5ZkFbYLD1xwsfoX/Z6N+oLiGL8wFsO45own0iywo+9MIj9fdbP9/UtPbmzjQlTmvp/6U3pW+kqDotZvKCJjJ6APRFvNDK/poCo8EuDphe63L2umFp+VsQ05/4GSdLfwveUEyh+V2NV41XwDdwKQUkpPppl99KigaPPpfPUwLusldIvOmMOzF82/e3Hne8eJTACv55Hkjji6SCAy0qF6TK8BLRxxRMFhjpkCUjWJc0dl96iNbw9AEHgE1LhTprhTo1KTNcYYa908/sCnnXLVyW3rpjzxjIEjf/998HyX6sxv4+xLAjC5q44PzidZy/uNyr0TDMbfglxtETDhxX1yWDf7CgvUkpeU1NljEqzNnq7PvU1pjJu+QQkK49zVEzyGN5WWCzRBsfvRmSbfudlxHUKe32EO5GDqKli8fU2JVLSbPrRdjnZMW47feO7Adi5Ftr39wFHks2coR7hjTdK/xo2JytwOIPXOJYwZfvlgYMITqwa3U9PEtEdGbDp/n3A8Tb8KDS7VL/2v6f3SZo2NW1ITvCNMUx4fWEXDWLKgOoE+rJo7kWDvI/3nj9o2NVy18BiTs7BQdQlZJ3c8GdMwd6NSDsomT0EpFJPgkyMJIiPDkri0DXsgHwGDCB+XAvCbYSlUpRJgH5iz+zG+jGV86lk35YZsowS3Hi+a1YX6BSd83W4rgwvlr/at+ORhEz+IMiWZChBBJDgxAGitqBHVaZ+cMhrWHRWh8M8DhkDBM+Jpv0ml0z/20hshz9cA+9Mt3qmuwPEU538By8QL5y5Ex7C0zNHe85CQlo2fvxW0fXWChP4wt1e6oQmvO0LMduPYDCHa0h4Hho+Bq01FsmAxCSr/kffvEY5/lvAbkrIyRzpwR1bOzx7AkGaGCx2xGQxTCMNZ9DanXp9VUg7vDhrUXcPjh8UJjxzUyWHoYm/7j7kaNdEVzetDsba1RwntRrXdyS4fmIsAkkflHq/WPNlx3fAAJ+ttIOyRA/61WC8GRFCwH6k/Naf5t7sK4/+92Nfnmg+noI+/CO4wkGUq4AvvexeBFDZNendebdSAFza6+BEqc9AwElXlN5Os+/QsTqr5a02l4ETjqLI6R11CV3SlNEyZlFxSi3DsOnxV5iRKJ6/xpgQFK0X0r6fsi9OYSIX7R7LYFkMChwJqgBEVsHZys1Y+xStKScIBPhfZTOuKzeQ0A9H/pNC5iluB2smlERltU9uVYb+zlX8UQ+qh0WlzYRb6rU2Pf1BT2afyffQPzF2YBqOhm7YzZFYTov4jnPDaiKWYeH8SXYQW8pWjDDCmxUZDRmqkEkhXD8w0jsjAQl4yQc9pLTcA92fJS7L5SQ5y/9P8D61dBbtJeJ43cesO/JOcECmUjahSBaTwqrXhqoq6Pcnc8qlVFIHhDysDy7MIetAzVTGl/OS0PnWXKtAp33RxWYF/FXon01F8X0tYDB9R7DRyNEoki04EJlVmcjvG02ZgrI4V0S9/V+oi9l3PJGU17Hvt5Vg+n7f1QJLGxWuRd33CPY0pojg3ay+qgnvkHcU2MtHHsKb5Ztk+2BgT9QdWsXhU7ZjgJ67E+WtlkKeixjRixvtrZHW7zYVK0aB0EyYYO+qJkjME7fWsx5cuBO2/uiM1NuIPYjFCGa91io4M+S/VfCxEcxCXLyZRE63I6ih10WXtP5RRLqVY8/NDfsm5BPuDJzDyFVwg1GXXCM/vAJd3MqZkDM1ykC1DY3HUziN27F5BbjV/rOaZCmH9cgn9UgfHxAFlJ6VXW4SumJaFpAvfihG0M0qL7B2ohCUnpseibblrEMN1kG7+evmIt5Jy76551kzpNiTOJ9gazMmMv4IRa8ftMJCWY6H/O9eMWamhhBl4Nv5ZNoTmyfc="
           $decryptData = {
               "challenge": "e28a9d37-d0dc-4d4d-acd9-5025d8e03328",
               "token": "WJF9bl6fry7a9xf82ystxgyj4jMoxV2x",
               "type": "url_verification"
           }
    */

    // https://open.feishu.cn/document/ukTMukTMukTM/uYDNxYjL2QTM24iN0EjN/event-subscription-configure-/encrypt-key-encryption-configuration-case
    // Encrypt Key 配置示例 - 如何使用
    private function decrypt($request)
    {
        $data = $request->all();
        $encryptData = $data['encrypt'];
        $key = hash('sha256', $this->encryptKey, true);
        $decryptData = openssl_decrypt(base64_decode($encryptData), 'AES-256-CBC', $key, OPENSSL_RAW_DATA, substr($key, 0, 16));
        $decryptData = json_decode(substr($decryptData, 16), true);
        return $decryptData;
    }
}
