<?php

namespace bxjm\EasyFeishu;

use bxjm\EasyFeishu\Server\Server;
use bxjm\EasyFeishu\Client\MiniProgramInApp as Miniprogram;
use bxjm\EasyFeishu\Client\H5InApp;

// 入口文件
// phpcs:disable Generic.Files.LineLength
class Manager
{
    public $server;
    public $miniprogram;

    public function __construct($config = [])
    {
        $this->server = new Server($config);
        $this->miniprogram = new Miniprogram($this->server);
        $this->h5 = new H5InApp($this->server);
    }

    // SDK初期调试：Server端确认
    public function basicTestOnServerSide()
    {
        echo '--- basicTestOnServerSide ---' . PHP_EOL;
        echo $this->server->tokenMgr->access_token() . PHP_EOL;  // 获取access_token
//        print_r($this->server->tenant->getTenantInfo('string'));  // 获取企业信息
//        print_r($this->server->contact->getUserFromUserId('chenhao'));  // 获取单个用户信息
//        print_r($this->server->contact->getUserFromCellphone('18513103328', 'open_id'));  // 使用手机号获取用户ID
//        print_r($this->server->contact->getUsers('', 'od-72bdcea3cc371a3752ec03398425b3fc'));
//        print_r($this->server->contact->getDepartments());  // 获取部门信息列表(历史)
//        print_r($this->server->contact->getChildrenDepartments()); // 获取子部门列表
//        print_r($this->server->im->sendMessage('ou_a443c5564d6955135abd2c938b5bb392', json_encode($this->_getTestMessageContent()), 'interactive', 'open_id'));  // 发送消息
//        print_r($this->server->im->replyMessage('om_7da0b263d0e1a49767b6d5733104b5b8', "{\"text\":\" 你好\"}", 'text'));  // 回复消息
//        print_r($this->server->im->withdrawMessage('om_655de3de80ff36a8f3facd23b26ee90f'));   // 撤回消息
//        print_r($this->server->im->messageReadUsers('om_7da0b263d0e1a49767b6d5733104b5b8', 'user_id')); // 查询消息已读信息
//        print_r($this->server->im->getHistoryMessages('chat', 'oc_1e2897b606da996eb5127f6db96dea34'));   // 获取会话历史消息
//        print_r($this->server->im->sendUrgentMessage('oc_1e2897b606da996eb5127f6db96dea34', ['59d34919']));
        //print_r($this->server->im->getSpecifyMessageContent('om_182b1e958ce716d1f167f26ae1b5e01f'));    // 获取指定消息内容
        //print_r($this->server->im->updateCardMessage('om_182b1e958ce716d1f167f26ae1b5e01f', json_encode($this->_getTestMessageContent())));  // 更新应用卡片消息

//        print_r($this->server->im->createChat('测试创建群3', ['user_id_list' => ['59d34919', 'e4c365f7'], 'bot_id_list' => ['cli_a10496c91738d00c', 'cli_a10496d869f9d013']]));  // 创建群
        //print_r($this->server->im->getChat('oc_41b7d5563770f45722372a8be3fa7c3f'));  // 获取群信息
        //print_r($this->server->im->addMemberToChat(['b6fb8df2'], 'oc_41b7d5563770f45722372a8be3fa7c3f'));  // 将用户或机器人拉入群聊
        //print_r($this->server->im->deleteMemberToChat(['b6fb8df2'], 'oc_41b7d5563770f45722372a8be3fa7c3f'));  // 将用户或机器人移出群聊
        //print_r($this->server->im->joinChat('oc_41b7d5563770f45722372a8be3fa7c3f'));  // 用户或机器人主动加入群聊
        //print_r($this->server->im->sendMessage('b6fb8df2', "{\"chat_id\":\"oc_41b7d5563770f45722372a8be3fa7c3f\"}", 'share_chat', 'string'));  // 发送消息
        //print_r($this->server->im->addMemberToChat(['kemengyu', 'yangxin', 'liubinghang', 'madechao', 'zhangpeng', 'zhangjiao', 'liuzhiyu', 'guoyajian', 'xuxinpeng'], 'oc_41b7d5563770f45722372a8be3fa7c3f'));  // 将用户或机器人拉入群聊
        //print_r($this->server->im->getChatMemberList('oc_41b7d5563770f45722372a8be3fa7c3f'));   // 获取群成员列表
        //print_r($this->server->im->getChatList());  // 获取用户或机器人所在的群列表
        //print_r($this->server->im->searchChatList('测试'));  // 搜索对用户或机器人可见的群列表
//        print_r($this->server->im->isInChat('oc_999684563c0ef6cfddee136c14fc1c82'));  // 判断用户或机器人是否在群里

        //print_r($this->server->im->updateChat(['name'=>'测试创建群API_2'], 'oc_41b7d5563770f45722372a8be3fa7c3f'));  // 获取群信息
        //print_r($this->server->im->sendTextMessageToChat('oc_41b7d5563770f45722372a8be3fa7c3f', 'hello here', 'string'));  // 发送消息
        //print_r($this->server->im->deleteChat('oc_41b7d5563770f45722372a8be3fa7c3f'));  // 解散群

//        echo PHP_EOL . '--- basicTestOnServerSide ---(审批相关)' . PHP_EOL;
//        print_r($this->server->approval->getDetailFromApprovalCode('C9CCF563-47EA-494D-956A-7EF3F3DDE115'));  // 查看审批定义 黑马
//        print_r($this->server->approval->getDetailFromApprovalCode('74CD811E-FE5A-4C39-A9AB-47A52B8D7D72'));  // 查看审批定义 bxjm
//        print_r($this->server->approval->batchGetInstanceIds('6A0E78C0-141E-4130-9277-A5862E89C8E6', '1631945482000', '1634548299000', 0));  // 批量获取审批实例
//        print_r($this->server->approval->getApprovalInstanceCodes('E895265D-F335-421E-8CC2-6783E746BFA0', '1631945482000', '1657268242000'));  // 批量获取审批实例
//         print_r($this->server->approval->createInstanceFromApprovalCode(
//             'E34E09E9-95E6-4A19-964E-F3DD44EDD41D',
//             'zhangpeng',
//             $this->_getTestFormData(),
//             'string'
//         ));  // 创建审批实例
        //3D86F126-F9C1-4B0F-885A-69A8B2D39539
//        $res = $this->server->approval->getDetailFromInstanceCode('72D9B77A-B1F9-4DF6-B1A6-4B2DA50122CD');
//        file_put_contents('/Users/zhangpeng/Sites/wwwroot/larkTest/json.txt', json_encode($res['data']['task_list']));
//        print_r($this->server->approval->getDetailFromInstanceCode('74674338-7876-4557-A8C7-82E644787520'));  // 获取单个审批实例详情
//        print_r($this->server->approval->approveInstance('E895265D-F335-421E-8CC2-6783E746BFA0', 'FE754883-0B54-4B81-97D8-614894D1E490', 'chenhao', '7117458391387078660'));  // 审批任务同意
//        print_r($this->server->approval->rejectInstance('6A0E78C0-141E-4130-9277-A5862E89C8E6', '705B7534-347B-47D8-8880-B0733FB6A19E', 'f8a71e25', '7020337828634673155')); // 审批任务拒绝
//        print_r($this->server->approval->transferInstance('6A0E78C0-141E-4130-9277-A5862E89C8E6', '89BC60E0-79B1-45A9-84DD-9FA1650777AD', 'cda7baf4', '7020360204214665218', 'eeg338a6', '测试'));  // 审批任务转交
//        print_r($this->server->approval->cancelInstance('E895265D-F335-421E-8CC2-6783E746BFA0', 'B9DE5DF3-6943-42BC-8E36-53B6CC319200', 'e4c365f7')); // 审批任务撤销
//        print_r($this->server->approval->ccInstance('6A0E78C0-141E-4130-9277-A5862E89C8E6', '746098F1-AFEC-49BB-BECE-E7179112A943', '2c1c77de', ['cda7baf4'])); // 审批实例抄送
//        print_r($this->server->approval->addSignToInstanceTask('BB245CB0-29CA-46BA-891F-AFC9597D81A3', 'D7EFA7E1-61FF-4846-B521-E35DB2ADC71E', 'gd1b45f8', '7089031982608400412', ['agf3ddd2'], 1, 1)); // 审批任务加签, 只能这个任务的审批人才可以操作
        // od-954788ccdaa2fcb1a94aa08da5af41e2
//        print_r($this->server->approval->previewInstance('2c1c77de', '746098F1-AFEC-49BB-BECE-E7179112A943', '7020347626734223361')); // 审批流程预览
//        print_r($this->server->approval->previewInstance('gd1b45f8', '', '', '9C86A740-CF39-4084-88C9-C2C07A27DEBB', '', $this->_getTestData())); // 审批流程预览
//        print_r($this->server->approval->queryTaskByUserOrTaskGroup('59d34919', 3));   // 用户角度列出任务
        //print_r($this->server->approval->searchInstancesByUserId('b6fb8df2', null, 'string')); // 实例列表查询
        //print_r($this->server->approval->searchTasksByUserId('b6fb8df2', null, 'string')); // 任务列表查询
        //print_r($this->server->approval->searchCcsByUserId('b6fb8df2', null, 'string')); // 抄送列表查询接口
//        print_r($this->server->approval->subscribeApproval('FA078692-BA78-4BB6-90BC-93326BA5D213')); // 订阅审批事件
//        print_r($this->server->approval->unsubscribeApproval('88CE9BB2-AA4E-4866-87BE-D9D5DFC35043'));  // 取消订阅审批事件
//        print_r($this->server->im->addChatManagers('oc_cf1864a5a55109dbd27d75f1dbad024d', ['gd38abbe'], 'user_id'));
//        print_r($this->server->ehr->batchGetEmployeesInfo('full', 0, 10, 2, '3e21354a'));
//        print_r($this->server->ehr->getAttachments('d93417814ef743a9806bd534081787b8'));  // 智能人事 下载附件
//        print_r($this->server->approval->uploadFile('test.jpg', 'image', '/Users/zhangpeng/Sites/wwwroot/larkTest/2.docx'));  //审批上传附件
//          print_r($this->server->im->uploadImage());
//        print_r($this->server->im->uploadImage($this->server->ehr->getAttachments('d93417814ef743a9806bd534081787b8')));
//        print_r($this->server->contact->getParentDepartments('od-77ecc18e32f86c99d5d7081fb4f59d4c')); // 获取部门父部门
        print_r($this->server->im->uploadFile('https://oss.tb.tokoyi.com/tb_pm_prj/1120/archive/%E6%B5%8B%E8%AF%95%E6%96%87%E6%A1%A3_1660717582140.pdf', '01100210061150623958.pdf', 'pdf'));
//        print_r($this->server->im->getFile('file_v2_a0bc667d-f358-4db9-8713-c246643839eg'));
        $instanceCode = '9C6D4523-352C-4459-B908-53175567AC00';
        $userId = '59d34919';
        $content = json_encode(['text' => '这是一条测试评论']);
        $atInfoList = [[
            'user_id' => 'chenhao',
            'name' => '陈皓',
            'offset' => 0
        ]];
//        print_r($this->server->approval->createInstanceComments($instanceCode, $userId, $content));
//        print_r($this->server->approval->getInstanceComments($instanceCode, $userId));
//        print_r($this->server->approval->deleteInstanceComments($instanceCode, $userId, '7122647932818210817'));
//        print_r($this->server->approval->removeInstanceComments($instanceCode, $userId));
    }

    // SDK初期调试：Client端确认
    public function basicTestOnClientSide($code = '')     // MiniProgramInApp
    {
        echo '--- basicTestOnClientSide ---' . PHP_EOL;
        echo $this->server->tokenMgr->access_token() . PHP_EOL;
//        print_r($this->h5->getUserInfoByAuthCode('N1bb5gIYUyUMkKBtajA7ye'));

//        print_r($this->miniprogram->getUserFromCode($code));
    }

//    private function _getTestFormData()
//    {
//        $data = [
//            [
//                'id' => 'widget16345470255720001',
//                'type' => 'input',
//                'value' => '1019测试'
//            ],
//            [
//                'id' => 'widget16345470399270001',
//                'type' => 'number',
//                'value' => 1442
//            ]
//        ];
//        return json_encode($data);
//    }

    private function _getTestData()
    {
        $data = [[
        "id"=> "widget16401607676930001",
	"type"=> "radioV2",
	"value"=> "407"
], [
        "id"=> "widget0",
	"type"=> "textarea",
	"value"=> "123"
], [
        "id"=> "widget1",
	"type"=> "amount",
	"value"=> "123"
], [
        "id"=> "widget16401608785310001",
	"type"=> "radioV2",
	"value"=> "kxh9ftfn-hh9wwqrc6jg-0"
], [
        "id"=> "widget16401609360670001",
	"type"=> "radioV2",
	"value"=> "kxh9h1tv-96f4o3hi1k9-0"
], [
        "id"=> "widget16401609809020001",
	"type"=> "radioV2",
	"value"=> "kxh9ibxz-1sd7cnxhpqc-1"
], [
        "id"=> "widget16385007828770001",
	"type"=> "text",
	"value"=> "隐蔽验收：付款金额≤合同金额*30%</br>中期验收：付款金额≤合同金额*70%</br>竣工验收：付款金额≤合同金额*95%</br>预付款：付款金额≤合同总金额*10%"
], [
        "id"=> "widget15754433486300001",
	"type"=> "radioV2",
	"value"=> "k3qyajs4-2iqoo6rx1gl-5"
], [
        "id"=> "widget16384439876790001",
	"type"=> "textarea",
	"value"=> "123"
], [
        "id"=> "widget16384440847450001",
	"type"=> "amount",
	"value"=> "123"
], [
        "id"=> "widget16384441071210001",
	"type"=> "amount",
	"value"=> "123"
], [
        "id"=> "widget16401610513480001",
	"type"=> "radioV2",
	"value"=> "2"
], [
        "id"=> "widget16401611440050001",
	"type"=> "radioV2",
	"value"=> "1"
], [
        "id"=> "widget16401614275540001",
	"type"=> "radioV2",
	"value"=> "1"
], [
        "id"=> "widget16384441995920001",
	"type"=> "input",
	"value"=> "213"
], [
        "id"=> "widget16385009792960001",
	"type"=> "text",
	"value"=> "《申请单》由起草人打印与其他单据一并提交计划财务中心费用会计"
]];
        return json_encode($data);
    }
     private function _getTestFormData()
    {
         $input_data = [
             [
                 "id" => "widget0",                          // 付款事由
                 "type" => "textarea",
                 "value" => "付款事由值",
             ],
             [
                 "id" => "widget16342838871720001",          // 项目名称
                 "type" => "radioV2",
                 "value" => "kus2frdj-dknjaa1nitr-1",
             ],
             [
                 "id" => "widget1",                          // 付款金额
                 "type" => "amount",
                 "value" => "7000",
             ],
             [
                 "id" => "widget15754433486300001",          // 付款方式
                 "type" => "radioV2",
                 "value" => "k3qyajs4-6lxuealpiu4-13", // 银行卡
             ],
             [
                 "id" => "widget3",                          // 付款日期
                 "type" => "date",
                 "value" => "2021-11-11T00:00:00+08:00",
             ],
             [
                 "id" => "widget16342837065570001",          // 收款单位
                 "type" => "radioV2",
                 "value" => "kus2bmjb-vhyf4hvxvub-0",
             ],
             [
                 "id" => "widget16346994169030001",                          // 开户银行
                 "type" => "input",
                 "value" => "甲银行",
             ],
             [
                 "id" => "widget16346994521260001",                          // 银行账户
                 "type" => "input",
                 "value" => "1111222233334444555",
             ],
             // [
             //     "id" => "widget15828099482720001",          // 附件
             //     "type" => "attachmentV2",
             //     "value" => "xxx",
             // ],

         ];
         return json_encode($input_data);
     }

    private function _getTestMessageContent()
    {
        $content = [
            "config" => [
                "wide_screen_mode" => true
            ],
            "elements" => [
                [
                    "alt" => [
                        "content" => "",
                        "tag" => "plain_text"
                    ],
                    "img_key" => "img_7ea74629-9191-4176-998c-2e603c9c5e8g",
                    "tag" => "img"
                ],
                [
                    "tag" => "div",
                    "text" => [
                        "content" => "更新一下？\n你有哪些想极力推荐给他人的珍藏书单？\n\n加入 **4·23 飞书读书节**，分享你的**挚爱书单**及**读书笔记**，**赢取千元读书礼**！\n\n📬 填写问卷，晒出你的珍藏好书\n😍 想知道其他人都推荐了哪些好书？马上[入群围观](https://open.feishu.cn/)\n📝 用[读书笔记模板](https://open.feishu.cn/)（桌面端打开），记录你的心得体会\n🙌 更有惊喜特邀嘉宾 4月12日起带你共读",
                        "tag" => "lark_md"
                    ]
                ],
                [
                    "actions" => [
                        [
                            "tag" => "button",
                            "text" => [
                                "content" => "立即推荐好书",
                                "tag" => "plain_text"
                            ],
                            "type" => "primary",
                            "url" => "https://open.feishu.cn/"
                        ], [
                            "tag" => "button",
                            "text" => [
                                "content" => "查看活动指南",
                                "tag" => "plain_text"
                            ],
                            "type" => "default",
                            "url" => "https://open.feishu.cn/"
                        ]],
                    "tag" => "action"
                ]
            ],
            "header" => [
                "template" => "turquoise",
                "title" => [
                    "content" => "📚晒挚爱好书，赢读书礼金",
                    "tag" => "plain_text"
                ]
            ]
        ];
        return $content;
    }
}




/*

TestApi.php
----------------------------------------
<?php
    require('vendor/autoload.php');
    $config = [
        'appId'      => 'xxxxxxxx',
        'secret'     => 'xxxxxxxx',
        'encryptKey' => 'xxxxxxxx',
    ];
    $code = 'xxxxxxxx';
    $manager = new \bxjm\EasyFeishu\Manager($config);
    $manager->basicTestOnServerSide();
    $manager->basicTestOnClientSide($code);

----------------------------------------


TestEvent.php
----------------------------------------
<?php
    require('vendor/autoload.php');
    $config = [
        'appId'      => 'xxxxxxxx',
        'secret'     => 'xxxxxxxx',
        'encryptKey' => 'xxxxxxxx',
    ];
    $manager = new \bxjm\EasyFeishu\Manager($config);
    $event_body = $manager->server->serve($request);

    if (array_key_exists('challenge', $event_body)) {
        return response($event_body, 200);
    }
    switch ($event_body['header']['event_type']) {
        // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/contact-v3/user/events/created
        // 员工入职
        case 'contact.user.created_v3':
            // do something
            return response(['code'=>200, 'msg'=>'ok', 'data'=>'收到员工入职事件'], 200);
            break;
        // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/contact-v3/user/events/updated
        // 员工变更
        case 'contact.user.updated_v3':
            // do something
            return response(['code'=>200, 'msg'=>'ok', 'data'=>'收到员工变更事件'], 200);
            break;
        // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/contact-v3/user/events/deleted
        // 员工离职
        case 'contact.user.deleted_v3':
            // do something
            return response(['code'=>200, 'msg'=>'ok', 'data'=>'收到员工离职事件'], 200);
            break;
        // 其它
        default:
            return response(['code'=>200, 'msg'=>'ok', 'data'=>'收到未知事件'], 200);
            break;
    }

----------------------------------------
*/
