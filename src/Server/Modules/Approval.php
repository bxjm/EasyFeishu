<?php

namespace bxjm\EasyFeishu\Server\Modules;

use bxjm\EasyFeishu\Client\Log\LoggerManager;

// https://open.feishu.cn/document/ukTMukTMukTM/ukDNyUjL5QjM14SO0ITN
// 审批
// phpcs:disable Generic.Files.LineLength
class Approval
{
    use \bxjm\EasyFeishu\Traits\HasHttpMgr;

    // https://open.feishu.cn/document/ukTMukTMukTM/uADNyUjLwQjM14CM0ITN
    // 查看审批定义
    public function getDetailFromApprovalCode($code, $format = 'array')
    {
        $absUrl = $this->feishu_oapi_domain_2 . '/approval/openapi/v2/approval/get';
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        $post_data = [
            'approval_code' => $code,
            'locale' => 'zh-CN'
        ];
        try {
            $response = $this->httpClient->post(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'json' => $post_data
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/ukTMukTMukTM/uQDOyUjL0gjM14CN4ITN
    // 批量获取审批实例(v2)
    public function batchGetInstanceIds($approvalCode, $startTime, $endTime, $offset, $limit = 20, $format = 'array')
    {
        $absUrl = $this->feishu_oapi_domain_2 . '/approval/openapi/v2/instance/list';
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        $post_data = [
            'approval_code' => $approvalCode,
            'start_time' => $startTime,
            'end_time' => $endTime,
            'offset' => $offset,
            'limit' => $limit
        ];
        try {
            $response = $this->httpClient->post(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'json' => $post_data
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/approval-v4/instance/list
    // 批量获取审批实例ID
    public function getApprovalInstanceCodes($approvalCode, $startTime, $endTime, $pageToken = '', $pageSize = 100, $format = 'array')
    {
        $absUrl = $this->feishu_oapi_domain_1 . '/approval/v4/instances';
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        $queries = [
            'approval_code' => $approvalCode,
            'start_time' => $startTime,
            'end_time' => $endTime,
            'page_size' => $pageSize,
            'page_token' => $pageToken,
        ];
        try {
            $response = $this->httpClient->get(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'query' => $queries,
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/ukTMukTMukTM/uIDNyUjLyQjM14iM0ITN
    // 创建审批实例
    public function createInstanceFromApprovalCode($code, $user_id, $formData, $nodeApprovalUserIdList = null, $nodeCcUserIdList = null, $format = 'array')
    {
        $absUrl = $this->feishu_oapi_domain_2 . '/approval/openapi/v2/instance/create';
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        $post_data = [
            'approval_code' => $code,
            'user_id' => $user_id,
            'open_id' => '',
            'form' => $formData,
            'node_approver_user_id_list' => $nodeApprovalUserIdList,
            'node_cc_user_id_list' => $nodeCcUserIdList
        ];
        try {
            $response = $this->httpClient->post(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'json' => $post_data
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/ukTMukTMukTM/uEDNyUjLxQjM14SM0ITN
    // 获取单个审批实例详情
    public function getDetailFromInstanceCode($code, $format = 'array')
    {
        $absUrl = $this->feishu_oapi_domain_2 . '/approval/openapi/v2/instance/get';
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        $post_data = [
            'instance_code' => $code,
            'locale' => 'zh-CN'
        ];
        $logId = strtoupper(md5(uniqid(rand(), true)));
        try {
            $response = $this->httpClient->post(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'json' => $post_data
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
            if (isset($this->config['log'])) {
                list($usec, $sec) = explode(" ", microtime());
                LoggerManager::createDailyDriver($this->config['log']['name'], $this->config['log']['path'])
                    ->info('apiLog', [
                    'log_id' => $logId,
                    'request_time' => '时间：' . date('Y-m-d H:i:s') . "毫秒时间戳" . ((float)$usec + (float)$sec),
                    'request_url' => $absUrl,
                    'request_header' => $headers,
                    'request_body' => $post_data,
                    'response_status' => $response->getStatusCode(),
                    'response_header' => $response->getHeaders(),
                    'response_body' => json_decode($res, true),
                ]);
            }
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/ukTMukTMukTM/uMDNyUjLzQjM14yM0ITN
    // 审批任务同意
    public function approveInstance($approval_code, $instance_code, $user_id, $task_id, $format = 'array')
    {
        $absUrl = $this->feishu_oapi_domain_2 . '/approval/openapi/v2/instance/approve';
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        $post_data = [
            'approval_code' => $approval_code,
            'instance_code' => $instance_code,
            'user_id' => $user_id,
            'task_id' => $task_id,
        ];
        try {
            $response = $this->httpClient->post(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'json' => $post_data
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/ukTMukTMukTM/uQDNyUjL0QjM14CN0ITN
    // 审批任务拒绝
    public function rejectInstance($approval_code, $instance_code, $user_id, $task_id, $format = 'array')
    {
        $absUrl = $this->feishu_oapi_domain_2 . '/approval/openapi/v2/instance/reject';
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        $post_data = [
            'approval_code' => $approval_code,
            'instance_code' => $instance_code,
            'user_id' => $user_id,
            'task_id' => $task_id,
        ];
        try {
            $response = $this->httpClient->post(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'json' => $post_data
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/ukTMukTMukTM/uUDNyUjL1QjM14SN0ITN
    // 审批任务转交
    // 审批过的任务不可以转交, 只能转交审批实例当前操作人的任务 $userId为审批人user_id,  $transferUserId为被转交人user_id
    public function transferInstance($approvalCode, $instanceCode, $userId, $taskId, $transferUserId, $comment = '', $format = 'array')
    {
        $absUrl = $this->feishu_oapi_domain_2 . '/approval/openapi/v2/instance/transfer';
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        $post_data = [
            'approval_code' => $approvalCode,
            'instance_code' => $instanceCode,
            'user_id' => $userId,
            'task_id' => $taskId,
            'transfer_user_id' => $transferUserId,
            'comment' => $comment
        ];
        try {
            $response = $this->httpClient->post(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'json' => $post_data
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/ukTMukTMukTM/uYDNyUjL2QjM14iN0ITN
    // 审批实例撤回(可以不传openID)
    public function cancelInstance($approvalCode, $instanceCode, $userId, $notifyStarter = false, $userIdType = 'user_id', $format = 'array')
    {
        $absUrl = $this->feishu_oapi_domain_1 . '/approval/v4/instances/cancel?user_id_type=' . $userIdType;
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        $post_data = [
            'approval_code' => $approvalCode,
            'instance_code' => $instanceCode,
            'user_id' => $userId,
            'notifyStarter' => $notifyStarter
        ];
        try {
            $response = $this->httpClient->post(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'json' => $post_data
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/ukTMukTMukTM/uADOzYjLwgzM24CM4MjN
    // 审批实例抄送
    public function ccInstance($approvalCode, $instanceCode, $userId, $ccUserIds, $comment = '', $format = 'array')
    {
        $absUrl = $this->feishu_oapi_domain_2 . '/approval/openapi/v2/instance/cc';
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        $post_data = [
            'approval_code' => $approvalCode,
            'instance_code' => $instanceCode,
            'user_id' => $userId,
            'cc_user_ids' => $ccUserIds,
            'comment' => $comment
        ];
        try {
            $response = $this->httpClient->post(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'json' => $post_data
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/ukTMukTMukTM/ukTM5UjL5ETO14SOxkTN/approval-task-addsign
    // 审批任务加签操作
    public function addSignToInstanceTask($approvalCode, $instanceCode, $userId, $taskId, $addSignUserIds, $addSignType, $approvalMethod = 1, $comment = '', $format = 'array')
    {
        $absUrl = $this->feishu_oapi_domain_1 . '/approval/v4/instances/add_sign';
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        $post_data = [
            'approval_code' => $approvalCode,
            'instance_code' => $instanceCode,
            'user_id' => $userId,
            'task_id' => $taskId,
            'comment' => $comment,
            'add_sign_user_ids' => $addSignUserIds,
            'add_sign_type' => $addSignType,
            'approval_method' => $approvalMethod
        ];
        try {
            $response = $this->httpClient->post(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'json' => $post_data
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/ukTMukTMukTM/ukTM5UjL5ETO14SOxkTN/approval-preview
    // 审批流程预览
    public function previewInstance($userId, $instanceCode = '', $taskId = '', $approvalCode = '', $departmentId = '', $form = '', $format = 'array')
    {
        $absUrl = $this->feishu_oapi_domain_1 . '/approval/v4/instances/preview?user_id_type=user_id';
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        if ($form) {
            $post_data = [
                'approval_code' => $approvalCode,
                'user_id' => $userId,
                'department_id' => $departmentId,
                'form' => $form
            ];
        } else {
            $post_data = [
                'user_id' => $userId,
                'instance_code' => $instanceCode,
                'task_id' => $taskId
            ];
        }
        try {
            $response = $this->httpClient->post(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'json' => $post_data
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/approval-v4/task/query
    // 用户角度列出任务
    // 查询的任务分组主题topic 有如下值
    //  1-待办审批  2-已办审批 3-已发起审批 17-未读知会 18-已读知会
    public function queryTaskByUserOrTaskGroup($userId, $topic = 1, $pageToken = '', $pageSize = 20, $userIdType = 'user_id', $format = 'array')
    {
        $absUrl = $this->feishu_oapi_domain_1 . '/approval/v4/tasks/query';
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        $queries = [
            'user_id' => $userId,
            'topic' => $topic,
            'user_id_type' => $userIdType,
            'page_size' => $pageSize,
            'page_token' => $pageToken,
        ];
        try {
            $response = $this->httpClient->get(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'query' => $queries,
                    'user_id_type' => $userIdType
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/ukTMukTMukTM/uQjMxYjL0ITM24CNyEjN
    // 实例列表查询
    public function searchInstancesByUserId($user_id = null, $status = null, $format = 'array', $limit = 50)
    {
        $absUrl = $this->feishu_oapi_domain_2 . '/approval/openapi/v2/instance/search';
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        $post_data = [
            'user_id' => $user_id,
            'instance_status' => $status,
            'limit' => $limit,
        ];
        try {
            $response = $this->httpClient->post(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'json' => $post_data
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/ukTMukTMukTM/uYjMxYjL2ITM24iNyEjN
    // 任务列表查询
    public function searchTasksByUserId($user_id = null, $status = null, $format = 'array', $limit = 50)
    {
        $absUrl = $this->feishu_oapi_domain_2 . '/approval/openapi/v2/task/search';
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        $post_data = [
            'user_id' => $user_id,
            'task_status' => $status,
            'limit' => $limit,
        ];
        try {
            $response = $this->httpClient->post(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'json' => $post_data
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/ukTMukTMukTM/uUjMxYjL1ITM24SNyEjN
    // 抄送列表查询接口
    public function searchCcsByUserId($user_id = null, $status = null, $format = 'array', $limit = 50)
    {
        $absUrl = $this->feishu_oapi_domain_2 . '/approval/openapi/v2/cc/search';
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        $post_data = [
            'user_id' => $user_id,
            'read_status' => $status,
            'limit' => $limit,
        ];
        try {
            $response = $this->httpClient->post(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'json' => $post_data
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/ukTMukTMukTM/ucDOyUjL3gjM14yN4ITN
    // 订阅审批事件
    public function subscribeApproval($approvalCode, $format = 'array')
    {
        $absUrl = $this->feishu_oapi_domain_2 . '/approval/openapi/v2/subscription/subscribe';
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        $post_data = [
            'approval_code' => $approvalCode
        ];
        try {
            $response = $this->httpClient->post(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'json' => $post_data
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/ukTMukTMukTM/ugDOyUjL4gjM14CO4ITN
    // 取消审批订阅事件
    public function unsubscribeApproval($approvalCode, $format = 'array')
    {
        $absUrl = $this->feishu_oapi_domain_2 . '/approval/openapi/v2/subscription/unsubscribe';
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        $post_data = [
            'approval_code' => $approvalCode
        ];
        try {
            $response = $this->httpClient->post(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'json' => $post_data
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/ukTMukTMukTM/uUDOyUjL1gjM14SN4ITN
    // 上传文件
    public function uploadFile($name, $type, $content, $format = 'array')
    {
        $absUrl = $this->feishu_oapi_domain_2 . '/approval/openapi/v2/file/upload';
        $headers = [
            'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            'enctype' => 'multipart/form-data',
        ];
        $multipart = [
            [
                'name'     => 'name',
                'contents' => $name,
            ],
            [
                'name'     => 'type',
                'contents' => $type
            ],
            [
                'name'     => 'content',
                'contents' => fopen($content, 'r')
            ],
        ];

        try {
            $response = $this->httpClient->post(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'multipart' => $multipart
                ]
            );

            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // 审批实例创建评论
    public function createInstanceComments($instanceId, $userId, $content = '', $atInfoList = [], $parentCommentId = null, $commentId = null, $disableBot = false, $extra = null, $format = 'array')
    {
        $absUrl = $this->feishu_oapi_domain_1 . '/approval/v4/instances/' . $instanceId . '/comments?user_id_type=user_id&user_id=' . $userId;
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        $post_data = [
            'content' => $content,
            'at_info_list' => $atInfoList,
            'parent_comment_id' => $parentCommentId,
            'comment_id' => $commentId,
            'disable_bot' => $disableBot,
            'extra' => $extra
        ];
        try {
            $response = $this->httpClient->post(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'json' => $post_data
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

     // 获取审批实例评论
    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/approval-v4/instance-comment/list
    public function getInstanceComments($instanceId, $userId, $pageToken = '', $pageSize = 50, $format = 'array')
    {
        $absUrl = $this->feishu_oapi_domain_1 . '/approval/v4/instances/' . $instanceId . '/comments?user_id_type=user_id&user_id=' . $userId;
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        try {
            $response = $this->httpClient->get(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
//                    'query' => $queries,
                    'page_size' => $pageSize,
                    'page_token' => $pageToken
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // 删除审批实例评论
    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/approval-v4/instance-comment/delete
    public function deleteInstanceComments($instanceId, $userId, $commentId, $format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/approval/v4/instances/' . $instanceId . '/comments/' . $commentId . '?user_id_type=user_id&user_id=' . $userId;
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        try {
            $response = $this->httpClient->delete(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // 清空审批实例评论
    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/approval-v4/instance-comment/remove
    public function removeInstanceComments($instanceId, $userId, $format = 'array')
    {
        $absUrl = $this->feishu_oapi_domain_1 . '/approval/v4/instances/' . $instanceId . '/comments/remove?user_id_type=user_id&user_id=' . $userId;
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        try {
            $response = $this->httpClient->post(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
//                    'json' => $post_data
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }
}
