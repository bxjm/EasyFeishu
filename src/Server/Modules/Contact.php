<?php

namespace bxjm\EasyFeishu\Server\Modules;

// https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/contact-v3/resources
// 通讯录
// phpcs:disable Generic.Files.LineLength
class Contact
{
    use \bxjm\EasyFeishu\Traits\HasHttpMgr;

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/contact-v3/user/get
    // 获取单个用户信息
    public function getUserFromUserId($user_id, $userIdType = 'user_id', $departmentIdType = 'open_department_id', $format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/contact/v3/users/' . $user_id . '?user_id_type=' . $userIdType . '&department_id_type=' . $departmentIdType;
        $headers = $this->defautHeaders + [
            'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
        ];
        //$post_data = [];
        try {
            $response = $this->httpClient->get(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/contact-v3/user/batch_get_id
    // 通过手机号或邮箱获取用户 ID
    public function getUserFromCellphone($cellphone, $userIdType = 'user_id', $format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/contact/v3/users/batch_get_id?user_id_type=' . $userIdType;
        $headers = $this->defautHeaders + [
            'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
        ];
        $post_data = [
            'mobiles' => [$cellphone]
        ];
        try {
            $response = $this->httpClient->post(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'json' => $post_data
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/contact-v3/department/get
    // 获取单个部门信息
    public function getDepartmentInfo($departmentId, $userIdType = 'user_id', $departmentIdType = 'open_department_id', $format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/contact/v3/departments/' . $departmentId;
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        $queries = [
            'user_id_type' => $userIdType,
            'department_id_type' => $departmentIdType,
        ];
        try {
            $response = $this->httpClient->get(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'query' => $queries
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/contact-v3/department/list
    // 通讯录 部门 获取部门信息列表(历史接口)
    public function getDepartments($pageToken = '', $parentDepartmentId = 0, $fetchChild = true, $departmentIdType = 'open_department_id', $userIdType = 'user_id', $pageSize = 20, $format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/contact/v3/departments';
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        $queries = [
            'user_id_type' => $userIdType,
            'page_token' => $pageToken,
            'page_size' => $pageSize,
            'department_id_type' => $departmentIdType,
            'parent_department_id' => $parentDepartmentId,
            'fetch_child' => $fetchChild,
        ];
        try {
            $response = $this->httpClient->get(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'query' => $queries
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/contact-v3/department/children
    // 获取子部门列表
    public function getChildrenDepartments($pageToken = '', $departmentId = 0, $fetchChild = true, $departmentIdType = 'open_department_id', $userIdType = 'user_id', $pageSize = 20, $format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/contact/v3/departments/' . $departmentId . '/children';
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        $queries = [
            'user_id_type' => $userIdType,
            'page_token' => $pageToken,
            'page_size' => $pageSize,
            'department_id_type' => $departmentIdType,
            'fetch_child' => $fetchChild,
        ];
        try {
            $response = $this->httpClient->get(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'query' => $queries
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/contact-v3/department/parent
    // 获取父部门信息
    public function getParentDepartments($departmentId, $pageToken = '', $userIdType = 'user_id', $departmentIdType = 'open_department_id', $pageSize = 20, $format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/contact/v3/departments/parent';
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        $queries = [
            'user_id_type' => $userIdType,
            'page_token' => $pageToken,
            'page_size' => $pageSize,
            'department_id_type' => $departmentIdType,
            'department_id' => $departmentId
        ];
        try {
            $response = $this->httpClient->get(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'query' => $queries
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = ['code'=>$e->getCode(), 'msg'=>'error', 'data'=>$e->getMessage()];
        }
        return $format=='string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/contact-v3/user/find_by_department
    // 获取部门直属用户列表
    public function getUsersByDepartment($departmentId, $pageToken = '', $userIdType = 'user_id', $departmentIdType = 'open_department_id', $pageSize = 20, $format='array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/contact/v3/users/find_by_department';
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        $queries = [
            'user_id_type' => $userIdType,
            'page_token' => $pageToken,
            'page_size' => $pageSize,
            'department_id_type' => $departmentIdType,
            'department_id' => $departmentId
        ];
        try {
            $response = $this->httpClient->get(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'query' => $queries
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = ['code'=>$e->getCode(), 'msg'=>'error', 'data'=>$e->getMessage()];
        }
        return $format=='string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/contact-v3/user/list
    // 通讯录 用户 获取用户列表 (历史版本)
    public function getUsers($pageToken = '', $departmentId = '', $userIdType = 'user_id', $departmentIdType = 'open_department_id', $pageSize = 20, $format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/contact/v3/users';
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        $queries = [
            'user_id_type' => $userIdType,
            'page_token' => $pageToken,
            'page_size' => $pageSize,
            'department_id_type' => $departmentIdType,
            'department_id' => $departmentId
        ];
        try {
            $response = $this->httpClient->get(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'query' => $queries
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }


    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/contact-v3/group/member_belong
    // 通讯录 用户组 查询用户所属用户组
    public function getUsersBelongGroup($memberId, $memberIdType = 'user_id', $groupType = '', $pageSize = 500, $pageToke = '', $format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/contact/v3/group/member_belong';
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        $queries = [
            'member_id' => $memberId,
            'member_id_type' => $memberIdType,
            'page_size' => $pageSize,
            'page_token' => $pageToke
        ];
        if ($groupType) {
            $queries['group_type'] = $groupType;
        }
        try {
            $response = $this->httpClient->get(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'query' => $queries
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/server-docs/contact-v3/group-member/simplelist
    // 通讯录 用户组成员 查询用户组成员列表
    public function getGroupMemberSimpleList($groupId, $memberType = 'user', $memberIdType = 'user_id', $pageSize = 100, $pageToke = '', $format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/contact/v3/group/' . $groupId . '/member/simplelist';
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        $queries = [
            'member_type' => $memberType,
            'member_id_type' => $memberIdType,
            'page_size' => $pageSize,
            'page_token' => $pageToke
        ];
        try {
            $response = $this->httpClient->get(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'query' => $queries
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }
}
