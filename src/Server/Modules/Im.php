<?php

namespace bxjm\EasyFeishu\Server\Modules;

use bxjm\EasyFeishu\Client\Log\LoggerManager;

// https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/im-v1/chat-id-description
// 消息与群组
// phpcs:disable Generic.Files.LineLength
class Im
{
    use \bxjm\EasyFeishu\Traits\HasHttpMgr;

    /*
    |--------------------------------------------------------------------------
    | 消息
    |--------------------------------------------------------------------------
    */

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/im-v1/message/create
    // 发送消息：receive_id_type=user_id
    //     消息类型
    //         文档：https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/im-v1/message/create_json
    //         包括：text、post、image、file、audio、media、sticker、interactive、share_chat、share_user
    public function sendMessage($userId, $message, $msgType = 'text', $receiveIdType = 'user_id', $format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/im/v1/messages' . '?receive_id_type=' . $receiveIdType;
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        $post_data = [
            'receive_id' => $userId,
            'msg_type' => $msgType,
            'content' => $message,
        ];
        $logId = strtoupper(md5(uniqid(rand(), true)));
        try {
            $response = $this->httpClient->post(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'json' => $post_data
                ]
            );
            // $res = $response->getBody()->getContents();
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
            if (isset($this->config['log'])) {
                LoggerManager::createDailyDriver($this->config['log']['name'], $this->config['log']['path'])
                    ->info('apiLog', [
                    'log_id' => $logId,
                    'request_time' => date('Y-m-d H:i:s'),
                    'request_url' => $absUrl,
                    'request_header' => $headers,
                    'request_body' => $post_data,
                    'exception' => $e->getMessage(),
                ]);
            }
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/ukTMukTMukTM/ucDO1EjL3gTNx4yN4UTM
    // 批量发送消息
    public function batchSendMessage($msgType, $content, $userIds = [], $format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/message/v4/batch_send';
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        if ($msgType == 'interactive') {
            $post_data = [
                'user_ids' => $userIds,
                'msg_type' => $msgType,
                'card' => $content,
            ];
        } else {
            $post_data = [
                'user_ids' => $userIds,
                'msg_type' => $msgType,
                'content' => $content,
            ];
        }
        try {
            $response = $this->httpClient->post(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'json' => $post_data
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/im-v1/message/create
    // 发送消息：receive_id_type=chat_id
    public function sendTextMessageToChat($chatId, $message, $format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/im/v1/messages' . '?receive_id_type=chat_id';
        $headers = $this->defautHeaders + [
            'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
        ];
        $post_data = [
            'receive_id' => $chatId,
            'msg_type' => 'text',
            'content' => "{\"text\":\"" . $message . "\"}",
        ];
        try {
            $response = $this->httpClient->post(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'json' => $post_data
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/im-v1/message/reply
    // 消息与群组  消息  回复消息
    public function replyMessage($messageId, $content, $messageType = 'text', $format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/im/v1/messages/' . $messageId . '/reply';
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        $post_data = [
            'content' => $content,
            'msg_type' => 'text',
        ];
        try {
            $response = $this->httpClient->post(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'json' => $post_data
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/im-v1/message/delete
    // 撤回消息
    public function withdrawMessage($messageId, $format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/im/v1/messages/' . $messageId;
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        try {
            $response = $this->httpClient->delete(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/im-v1/message/read_users
    // 查询消息已读信息 (只能查询机器人自己发送，且发送时间不超过7天的消息)
    public function messageReadUsers($messageId, $userIdType, $pageToken = '', $pageSize = 20, $format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/im/v1/messages/' . $messageId;
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        $queries = [
            'user_id_type' => $userIdType,
            'page_token' => $pageToken,
            'page_size' => $pageSize
        ];
        try {
            $response = $this->httpClient->get(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'query' => $queries
                ]
            );
        } catch (\Exception $e) {
            // TBD
        }
        $response->getBody()->rewind();
        $res = $response->getBody()->getContents();
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/im-v1/message/list
    // 获取会话历史消息
    public function getHistoryMessages($containerIdType, $containerId, $startTime = '', $endTime = '', $pageToken = '', $pageSize = 20, $format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/im/v1/messages';
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        $queries = [
            'container_id_type' => $containerIdType,
            'container_id' => $containerId,
            'start_time' => $startTime,
            'end_time' => $endTime,
            'page_token' => $pageToken,
            'page_size' => $pageSize
        ];
        try {
            $response = $this->httpClient->get(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'query' => $queries
                ]
            );
        } catch (\Exception $e) {
            // TBD
        }
        $response->getBody()->rewind();
        $res = $response->getBody()->getContents();
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/im-v1/message/get
    // 消息与群组 消息 获取指定消息的内容
    public function getSpecifyMessageContent($messageId, $format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/im/v1/messages/' . $messageId;
        $headers = $this->defautHeaders + [
            'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
        ];
        try {
            $response = $this->httpClient->get(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                ]
            );
        } catch (\Exception $e) {
            // TBD
        }
        $response->getBody()->rewind();
        $res = $response->getBody()->getContents();
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/im-v1/message/patch
    // 消息与群组消息 - 消息卡片 - 更新应用发送的消息
    public function updateCardMessage($messageId, $content, $format = 'array')
    {
        $absUrl = $this->feishu_oapi_domain_1 . '/im/v1/messages/' . $messageId;
        $headers = $this->defautHeaders + [
            'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
        ];
        $post_data = [
            'content' => $content
        ];
        try {
            $response = $this->httpClient->patch(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'json' => $post_data
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    /*
    |--------------------------------------------------------------------------
    | 群组
    |--------------------------------------------------------------------------
    */

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/im-v1/chat/create
    // 创建群
    public function createChat($name, $options = [], $userIdType = 'user_id', $format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/im/v1/chats?user_id_type=' . $userIdType;
        $headers = $this->defautHeaders + [
            'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
        ];
        $post_data = [
            'name' => $name,
            'avatar' => $options['avatar'] ?? '',
            'description' => $options['description'] ?? '',
            'owner_id' => $options['owner_id'] ?? '',
//            'i18n_names' => $options['i18n_names'] ?? ['zh_cn' => $name],
            'user_id_list' => $options['user_id_list'] ?? [],
            'chat_type' => $options['chat_type'] ?? 'private',
            'external' => $options['external'] ?? false,
            'bot_id_list' => $options['bot_id_list'] ?? [],
            'join_message_visibility' => $options['join_message_visibility'] ?? 'all_members',
            'leave_message_visibility' => $options['leave_message_visibility'] ?? 'all_members',
            'membership_approval' => $options['membership_approval'] ?? 'no_approval_required',
            'chat_mode' => $options['chat_mode'] ?? 'group',
        ];
        try {
            $response = $this->httpClient->post(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'json' => $post_data
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/im-v1/chat/get
    // 获取群信息
    public function getChat($chat_id, $format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/im/v1/chats/' . $chat_id;
        $headers = $this->defautHeaders + [
            'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
        ];
        //$post_data = [];
        try {
            $response = $this->httpClient->get(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/im-v1/chat/update
    // 更新群信息
    public function updateChat($changes, $chat_id, $format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/im/v1/chats/' . $chat_id;
        $headers = $this->defautHeaders + [
            'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
        ];
        $post_data = $changes;
        try {
            $response = $this->httpClient->put(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'json' => $post_data
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/im-v1/chat/delete
    // 解散群
    public function deleteChat($chat_id, $format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/im/v1/chats/' . $chat_id;
        $headers = $this->defautHeaders + [
            'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
        ];
        //$post_data = [];
        try {
            $response = $this->httpClient->delete(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/im-v1/chat-moderation/update
    //更新群发言权限
    public function updateChatModeration($chartId, $moderatorAddedList = [], $moderatorRemovedList = [], $moderationSetting = 'moderator_list', $userIdType = 'user_id', $format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/im/v1/chats/' . $chartId . '/moderation?user_id_type=' . $userIdType;
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        $post_data = [
            'moderation_setting' => $moderationSetting,
            'moderator_added_list' => $moderatorAddedList,
            'moderator_removed_list' => $moderatorRemovedList
        ];
        try {
            $response = $this->httpClient->put(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'json' => $post_data
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    /*
    |--------------------------------------------------------------------------
    | 群组 - 群成员
    |--------------------------------------------------------------------------
    */

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/im-v1/chat-members/create
    // 将用户或机器人拉入群聊
    public function addMemberToChat($user_id_list, $chat_id, $member_id_type = 'user_id', $format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/im/v1/chats/' . $chat_id . '/members' . '?member_id_type=' . $member_id_type;
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        $post_data = [
            'id_list' => $user_id_list
        ];
        try {
            $response = $this->httpClient->post(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'json' => $post_data
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/im-v1/chat-members/delete
    // 将用户或机器人移出群聊
    public function deleteMemberToChat($user_id_list, $chat_id, $format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/im/v1/chats/' . $chat_id . '/members' . '?member_id_type=user_id';
        $headers = $this->defautHeaders + [
            'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
        ];
        $post_data = [
            'id_list' => $user_id_list
        ];
        try {
            $response = $this->httpClient->delete(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'json' => $post_data
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/im-v1/chat-members/me_join
    // 用户或机器人主动加入群聊
    public function joinChat($chat_id, $format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/im/v1/chats/' . $chat_id . '/members/me_join';
        $headers = $this->defautHeaders + [
            'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
        ];
        //$post_data = [];
        try {
            $response = $this->httpClient->patch(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/im-v1/chat-members/get
    // 获取群成员列表
    public function getChatMemberList($chat_id, $pageToken = '', $pageSize = 20, $memberIdType = 'user_id', $format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/im/v1/chats/' . $chat_id . '/members';
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        $queries = [
            'member_id_type' => $memberIdType,
            'page_token' => $pageToken,
            'page_size' => $pageSize
        ];
        try {
            $response = $this->httpClient->get(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'query' => $queries
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/im-v1/chat/list
    // 获取用户或机器人所在的群列表
    public function getChatList($format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/im/v1/chats/' . '?user_id_type=user_id';
        $headers = $this->defautHeaders + [
            'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
        ];
        //$post_data = [];
        try {
            $response = $this->httpClient->get(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/im-v1/chat/search
    // 搜索对用户或机器人可见的群列表
    public function searchChatList($keyword, $format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/im/v1/chats/search?query=' . $keyword;
        $headers = $this->defautHeaders + [
            'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
        ];
        //$post_data = [];
        try {
            $response = $this->httpClient->get(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/im-v1/chat-members/is_in_chat
    // 判断用户或机器人是否在群里
    public function isInChat($chat_id, $format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/im/v1/chats/' . $chat_id . '/members/is_in_chat';
        $headers = $this->defautHeaders + [
            'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
        ];
        //$post_data = [];
        try {
            $response = $this->httpClient->get(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/im-v1/chat-managers/add_managers
    // 指定群管理员
    public function addChatManagers($chatId, $managerIds = [], $memberIdType = 'user_id', $format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/im/v1/chats/' . $chatId . '/managers/add_managers?member_id_type=' . $memberIdType;
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        $post_data = [
            'manager_ids' => $managerIds
        ];
        try {
            $response = $this->httpClient->post(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'json' => $post_data
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }


    /*
    |--------------------------------------------------------------------------
    | 群组 - 群公告
    |--------------------------------------------------------------------------
    */

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/im-v1/chat-announcement/get
    // 获取群公告信息
    public function getChatAnnouncement($chat_id, $format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/im/v1/chats/' . $chat_id . '/announcement';
        $headers = $this->defautHeaders + [
            'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
        ];
        //$post_data = [];
        try {
            $response = $this->httpClient->get(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/im-v1/chat-announcement/patch
    // 更新群公告信息
    // TestData:
    //     $announcement = "{\"requestType\":\"InsertBlocksRequestType\",\"insertBlocksRequest\":{\"payload\":\"{\\\"blocks\\\":[{\\\"type\\\":\\\"paragraph\\\",\\\"paragraph\\\":{\\\"elements\\\":[{\\\"type\\\":\\\"textRun\\\",\\\"textRun\\\":{\\\"text\\\":\\\"Docs API Sample Content\\\",\\\"style\\\":{}}}],\\\"style\\\":{}}},{\\\"type\\\":\\\"table\\\",\\\"table\\\":{\\\"rowSize\\\":3, \\\"columnSize\\\":3}},{\\\"type\\\":\\\"sheet\\\",\\\"sheet\\\":{\\\"rowSize\\\":3, \\\"columnSize\\\":3}}]}\",\"location\":{\"zoneId\":\"0\",\"index\":0, \"endOfZone\": true}}}";

    public function setChatAnnouncement($chat_id, $announcement, $format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/im/v1/chats/' . $chat_id . '/announcement';
        $headers = $this->defautHeaders + [
            'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
        ];
        $post_data = [
            'revision' => '0',
            'requests' => $announcement
        ];
        try {
            $response = $this->httpClient->patch(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'json' => $post_data
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/im-v1/image/create
    // 上传图片
    public function uploadImage($image, $imageType = 'message', $format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/im/v1/images';
        $headers = [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
                'enctype' => 'multipart/form-data',
            ];
        $multipart = [
            [
                'name'     => 'image',
                'contents' => $image
            ],
            [
                'name'     => 'image_type',
                'contents' => $imageType,
            ]
        ];

        try {
            $response = $this->httpClient->post(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'multipart' => $multipart
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/im-v1/message/urgent_app
    // 发送应用内加急消息
//    public function sendUrgentMessage($messageId, $userIdList, $userIdType = 'user_id', $format = 'array')
//    {
//        $absUrl =  $this->feishu_oapi_domain_1 . '/im/v1/messages/' . $messageId . '/urgent_app?user_id_type=' . $userIdType;
//        $headers = $this->defautHeaders + [
//                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token()
//            ];
//        $postData = [
//            'user_id_list' => $userIdList
//        ];
//
//        try {
//            $response = $this->httpClient->patch(
//                $absUrl,
//                $this->defautGuzzleOption + [
//                    'headers' => $headers,
//                    'json' => $postData
//                ]
//            );
//            $res = $response->getBody()->getContents();
//        } catch (\Exception $e) {
//            // TBD
//            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
//        }
//        return $format == 'string' ? $res : json_decode($res, true);
//    }

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/im-v1/file/create
    // 上传文件
    public function uploadFile($file, $fileName, $fileType, $duration = 0, $format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/im/v1/files';
        $headers = [
            'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            'enctype' => 'multipart/form-data',
        ];
        $multipart = [
            [
                'name'     => 'file',
                'contents' => fopen($file, 'r')
            ],
            [
                'name'     => 'file_type',
                'contents' => $fileType,
            ],
            [
                'name'     => 'file_name',
                'contents' => $fileName,
            ],
            [
                'name'     => 'duration',
                'contents' => $duration,
            ]
        ];

        try {
            $response = $this->httpClient->post(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'multipart' => $multipart
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/im-v1/file/get
    // 下载文件
    public function getFile($fileKey)
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/im/v1/files/' . $fileKey;
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        try {
            $response = $this->httpClient->get(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers
                ]
            );
        } catch (\Exception $e) {
            // TBD
        }
        $response->getBody()->rewind();
        $res = $response->getBody()->getContents();
        return $res;
    }

    //
    //
    public function sendEphemeralCard($chatId, $userId, $card, $format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/ephemeral/v1/send';
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        $post_data = [
            'chat_id' => $chatId,
            'user_id' => $userId,
            'msg_type' => 'interactive',
            'card' => $card
        ];
        try {
            $response = $this->httpClient->post(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'json' => $post_data
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }
}
