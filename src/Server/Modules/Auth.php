<?php

namespace bxjm\EasyFeishu\Server\Modules;

use bxjm\EasyFeishu\Client\Log\LoggerManager;
use GuzzleHttp\Psr7\Stream;
use bxjm\EasyFeishu\Server\Modules\Tenant;
use bxjm\EasyFeishu\Server\Modules\Contact;
use bxjm\EasyFeishu\Server\Modules\Im;
use bxjm\EasyFeishu\Server\Modules\Approval;

// https://open.feishu.cn/document/ukTMukTMukTM/uMTNz4yM1MjLzUzM
// API访问凭证 (access token) - API访问凭证概述
// phpcs:disable PSR1.Methods.CamelCapsMethodName
class Auth
{
    // use \bxjm\EasyFeishu\Traits\HasHttpMgr;
    use \bxjm\EasyFeishu\Traits\HasHttpMgr {
        \bxjm\EasyFeishu\Traits\HasHttpMgr::__construct as private __HasHttpMgrTraitsConstruct;
    }

    private $accessTokenKey = 'app_access_token';
    private $expiresInKey = 'expire';
    private $appId = '';
    private $secret = '';
    private $appTicket = '';
    private $tenantKey = '';
    //
    private $access_token;
    private $expire_at = 0;

    public function __construct($config)
    {
        $this->__HasHttpMgrTraitsConstruct('', $config);
        $this->appId      = array_key_exists('appId', $config) ? $config['appId'] : '';
        $this->secret     = array_key_exists('secret', $config) ? $config['secret'] : '';
        $this->appTicket  = array_key_exists('appTicket', $config) ? $config['appTicket'] : '';
        $this->tenantKey  = array_key_exists('tenantKey', $config) ? $config['tenantKey'] : '';
        // $this->httpClient = new \GuzzleHttp\Client();
    }


    // token
    //     有效期为 2 小时，在此期间调用该接口 token 不会改变。
    //     当 token 有效期小于 30 分的时候，再次请求获取 token 的时候，会生成一个新的 token，
    //     与此同时老的 token 依然有效。
    //
    //     自建应用的 app_access_token 也代表了访问本租户内的相关API及资源，
    //     故，自建应用的 app_access_token 等同于 tenant_access_token
    //
    public function access_token()
    {
        $now = date_timestamp_get(date_create());
        if ($this->expire_at == 0 || ($this->expire_at - $now) < 60 * 30) {
            $this->access_token = $this->app_access_token_internal()['access_token'];
            $this->expire_at = $now + $this->app_access_token_internal()[$this->expiresInKey];
        }
        return $this->access_token;
    }

    // https://open.feishu.cn/document/ukTMukTMukTM/ukDNz4SO0MjL5QzM/auth-v3/auth/app_access_token_internal
    // API访问凭证(access token) - 获取 app_access_token（企业自建应用）
    private function app_access_token_internal()
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/auth/v3/app_access_token/internal';
        $headers = $this->defautHeaders;
        $post_data = [
            'app_id' => $this->appId,
            'app_secret' => $this->secret,
        ];
        try {
            $response = $this->httpClient->post(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'json' => $post_data
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Throwable $e) {
            if (isset($this->config['log'])) {
                LoggerManager::createDailyDriver($this->config['log']['name'], $this->config['log']['path'])
                    ->info('apiLog', [
                        'request_time' => date('Y-m-d H:i:s'),
                        'request_url' => $absUrl,
                        'request_header' => $headers,
                        'request_body' => $post_data,
                        'exception' => $e->getMessage(),
                        'file' => $e->getFile(),
                        'line' => $e->getLine(),
                        'code' => $e->getCode(),
                    ]);
            }
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }

        return $this->normalizeAccessTokenResponse($res);
    }

    // https://open.feishu.cn/document/ukTMukTMukTM/ukDNz4SO0MjL5QzM/auth-v3/auth/app_access_token
    // API访问凭证(access token) - 获取 app_access_token（应用商店应用）
    private function app_access_token_marketplace()
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/auth/v3/app_access_token';
        $headers = $this->defautHeaders;
        $post_data = [
            'app_id' => $this->appId,
            'app_secret' => $this->secret,
            'app_ticket' => $this->appTicket,
        ];
        try {
            $response = $this->httpClient->post(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'json' => $post_data
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Throwable $e) {
            if (isset($this->config['log'])) {
                LoggerManager::createDailyDriver($this->config['log']['name'], $this->config['log']['path'])
                    ->info('apiLog', [
                        'request_time' => date('Y-m-d H:i:s'),
                        'request_url' => $absUrl,
                        'request_header' => $headers,
                        'request_body' => $post_data,
                        'exception' => $e->getMessage(),
                        'file' => $e->getFile(),
                        'line' => $e->getLine(),
                        'code' => $e->getCode(),
                    ]);
            }
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }

        return $this->normalizeAccessTokenResponse($res);
    }

    // https://open.feishu.cn/document/ukTMukTMukTM/ukDNz4SO0MjL5QzM/auth-v3/auth/tenant_access_token_internal
    // API访问凭证 (access token) - 获取 tenant_access_token（企业自建应用）
    private function tenant_access_token_internal()
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/auth/v3/tenant_access_token/internal';
        $headers = $this->defautHeaders;
        $post_data = [
            'app_id' => $this->appId,
            'app_secret' => $this->secret,
        ];
        try {
            $response = $this->httpClient->post(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'json' => $post_data
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }

        return $this->normalizeAccessTokenResponse($res);
    }

    // https://open.feishu.cn/document/ukTMukTMukTM/ukDNz4SO0MjL5QzM/auth-v3/auth/tenant_access_token
    // API访问凭证 (access token) - 获取 tenant_access_token（应用商店应用）
    private function tenant_access_token_marketplace()
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/auth/v3/tenant_access_token';
        $headers = $this->defautHeaders;
        $post_data = [
            'app_access_token' => $this->app_access_token_marketplace()->app_access_token,
            'tenant_key' => $this->tenantKey,
        ];
        try {
            $response = $this->httpClient->post(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'json' => $post_data
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }

        return $this->normalizeAccessTokenResponse($res);
    }

    // https://open.feishu.cn/document/ukTMukTMukTM/ukDNz4SO0MjL5QzM/auth-v3/auth/app_ticket_resend
    // API访问凭证 (access token) - 重新推送 app_ticket
    private function app_ticket_resend()
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/auth/v3/app_ticket/resend';
        $headers = $this->defautHeaders;
        $post_data = [
            'app_id' => $this->appId,
            'app_secret' => $this->secret,
        ];
        try {
            $response = $this->httpClient->post(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'json' => $post_data
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = ['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()];
        }
        return $this->normalizeAccessTokenResponse($res);
    }

    // normalize
    private function normalizeAccessTokenResponse($response): array
    {
        if ($response instanceof Stream) {
            $response->rewind();
            $response = $response->getContents();
        }
        if (\is_string($response)) {
            $response = json_decode($response, true) ?? [];
        }
        if (!\is_array($response)) {
            throw new \bxjm\EasyFeishu\Exception('Invalid token response');
        }
        if (empty($response[$this->accessTokenKey])) {
            throw new \bxjm\EasyFeishu\Exception('Authorize Failed', 1);
        }
        return $response + [
                'access_token' => $response[$this->accessTokenKey],
                'expires_in' => \intval($response[$this->expiresInKey] ?? 0),
            ];
    }
}
