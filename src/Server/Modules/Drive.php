<?php

namespace bxjm\EasyFeishu\Server\Modules;

// https://open.feishu.cn/document/ukTMukTMukTM/uczNzUjL3czM14yN3MTN
// https://open.feishu.cn/document/ukTMukTMukTM/uUDN04SN0QjL1QDN/files/guide/introduction
// 云文档 - 文件管理
class Drive
{
    use \bxjm\EasyFeishu\Traits\HasHttpMgr;

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/drive-v1/file/upload_all
    // 上传文件 文件大小,全量上传最大20M
    public function uploadAll($fileName, $file, $parentNode, $size, $parentType = 'explorer', $format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/drive/v1/files/upload_all';
        $headers = [
            'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            'enctype' => 'multipart/form-data',
        ];
        $multipart = [
            [
                'name'     => 'file_name',
                'contents' => $fileName,
            ],
            [
                'name'     => 'file',
                'contents' =>  $file
            ],
            [
                'name'     => 'parent_node',
                'contents' => $parentNode
            ],
            [
                'name'     => 'size',
                'contents' => $size
            ],

            [
                'name'     => 'parent_type',
                'contents' => $parentType
            ],
        ];

        try {
            $response = $this->httpClient->post(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'multipart' => $multipart
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/ukTMukTMukTM/uEjNzUjLxYzM14SM2MTN
    // 获取文件夹下文档清单
    public function folderChildren(string $folderToken, array $types = ['doc', 'docx', 'sheet', 'file', 'bitable', 'folder'], $format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/drive/explorer/v2/folder/' . $folderToken . '/children';
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        $queries = [
            'types' => $types
        ];
        try {
            $response = $this->httpClient->get(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'query' => $queries
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/drive-v1/file/delete
    // 删除文件
    public function deleteFile(string $fileToken, string $type, $format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/drive/v1/files/' . $fileToken;
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        $queries = [
            'type' => $type
        ];
        try {
            $response = $this->httpClient->delete(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'query' => $queries
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }
}
