<?php

namespace bxjm\EasyFeishu\Server\Modules;

// https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/tenant-v2/tenant/query
// 企业信息
class Tenant
{
    use \bxjm\EasyFeishu\Traits\HasHttpMgr;

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/tenant-v2/tenant/query
    // 获取企业信息
    public function getTenantInfo($format = 'array')
    {
        $absUrl =  $this->feishu_oapi_domain_1 . '/tenant/v2/tenant/query';
        $headers = $this->defautHeaders + [
            'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
        ];
        //$post_data = [];
        try {
            $response = $this->httpClient->get(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers
                ],
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            // TBD
            $res = ['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()];
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }
}
