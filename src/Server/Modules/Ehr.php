<?php

namespace bxjm\EasyFeishu\Server\Modules;

// https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/ehr/ehr-v1/introduction
// 智能人事
class Ehr
{
    use \bxjm\EasyFeishu\Traits\HasHttpMgr;

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/ehr/ehr-v1/employee/list
    // 批量获取员工花名册信息
    public function batchGetEmployeesInfo($view = 'full', $pageToken = '0', $pageSize = 10, $status = [], $userIds = [], $userIdType = 'user_id', $format = 'array')
    {
        $absUrl = $this->feishu_oapi_domain_1 . '/ehr/v1/employees';
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        $queries = [
            'view' => $view,
            'status' => $status,
            'page_token' => $pageToken,
            'page_size' => $pageSize,
            'user_ids' => $userIds,
            'user_id_type' => $userIdType,
        ];
        try {
            $response = $this->httpClient->get(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers,
                    'query' => $queries,
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $format == 'string' ? $res : json_decode($res, true);
    }

    // https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/ehr/ehr-v1/attachment/get
    // 下载附件
    public function getAttachments($fileToken)
    {
        $absUrl = $this->feishu_oapi_domain_1 . '/ehr/v1/attachments/' . $fileToken;
        $headers = $this->defautHeaders + [
                'Authorization' => 'Bearer ' . $this->tokenMgr->access_token(),
            ];
        try {
            $response = $this->httpClient->get(
                $absUrl,
                $this->defautGuzzleOption + [
                    'headers' => $headers
                ]
            );
            $response->getBody()->rewind();
            $res = $response->getBody()->getContents();
        } catch (\Exception $e) {
            $res = json_encode(['code' => $e->getCode(), 'msg' => 'error', 'data' => $e->getMessage()]);
        }
        return $res;
    }
}
