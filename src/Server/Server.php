<?php

namespace bxjm\EasyFeishu\Server;

use bxjm\EasyFeishu\Server\Modules\Auth;

class Server
{
    use \bxjm\EasyFeishu\Traits\HasEventHandler;

    public $tokenMgr;
    public $config;

    protected $modules = [
        'tenant'    => Modules\Tenant::class,
        'contact'   => Modules\Contact::class,
        'im'        => Modules\Im::class,
        'approval'  => Modules\Approval::class,
        'ehr'       => Modules\Ehr::class,
        'drive'     => Modules\Drive::class
    ];

    public function __construct($config = [])
    {
        $this->tokenMgr = new Auth($config);
        $this->config = $config;
        $this->encryptKey = array_key_exists('encryptKey', $config) ? $config['encryptKey'] : '';
    }

    // public function __get($module)
    // {
    //     $target_module = preg_grep("/{$module}$/i" , $this->modules);
    //     if ($target_module!=false && count($target_module)==1) {
    //         $className = array_values($target_module)[0];
    //         $this->$module = new $className($this->tokenMgr);
    //     }
    //     return $this->$module;
    // }

    public function __get($moduleKeyName)
    {
        if (array_key_exists($moduleKeyName, $this->modules)) {
            $className = $this->modules[$moduleKeyName];
            $this->$moduleKeyName = new $className($this->tokenMgr, $this->config);
            return $this->$moduleKeyName;
        }
        return null;
    }
}
