## bxjm/EasyFeishu

A PHP SDK for Feishu
## Install

Via Composer

``` bash
$ composer require bxjm/easyfeishu
```

## Usage (API)

``` php
<?php

$config = [
        'appId'      => 'xxxxxxxx', // 可在【开放平台-开发者后台-...-凭证与基础信息】获取
        'secret'     => 'xxxxxxxx', // 可在【开放平台-开发者后台-...-凭证与基础信息】获取
    ];
$userId = 'xxxxxxxx';               // 可在【管理后台-组织架构-...-查看详情-用户ID】获取
$code   = 'xxxxxxxx';               // 通过小程序调用tt.login()获取（有效期3分钟，只能使用一次）

$manager = new \bxjm\EasyFeishu\Manager($config);
$server = $manager->server;
$client = $manager->miniprogram;

$server->tenant->getTenantInfo();                                   // 获取企业信息
$server->contact->getUserFromUserId($userId);                       // 获取单个用户信息
$server->im->sendMessage($userId, "{\"text\":\" hello\"}");         // 发送消息

$client->miniprogram->getUserFromCode($code);                       // code2session
```

## Usage (Event Handler)

``` php
<?php

$config = [
        'appId'      => 'xxxxxxxx', // 可在【开放平台-开发者后台-...-凭证与基础信息】获取
        'secret'     => 'xxxxxxxx', // 可在【开放平台-开发者后台-...-凭证与基础信息】获取
        'encryptKey' => 'xxxxxxxx', // 可在【开放平台-开发者后台-...-事件订阅】获取
    ];

$manager = new \bxjm\EasyFeishu\Manager($config);
$server = $manager->server;

$event_body = $server->serve($request);
//print_r($event_body);

if (array_key_exists('challenge', $event_body)) {
    return response($event_body, 200);
}
switch ($event_body['header']['event_type']) {
    case 'contact.user.created_v3':
        // do something
        return response(['code' => 200, 'msg' => 'OK', 'data' => '收到员工入职事件'], 200);
        break;
    case 'contact.user.updated_v3':
        // do something
        return response(['code' => 200, 'msg' => 'OK', 'data' => '收到员工变更事件'], 200);
        break;
    case 'contact.user.deleted_v3':
        // do something
        return response(['code' =>2 00, 'msg' => 'OK', 'data' => '收到员工离职事件'], 200);
        break;
    default:
        // do something
        return response(['code' => 200, 'msg' => 'OK', 'data' => '收到未知事件'], 200);
        break;
}
```

## Structure

If any of the following are applicable to your project, then the directory structure should follow industry best practices by being named the following.

```
.
|____Server
| |____Server.php
| |____Modules
| | |____Translation.php
| | |____Sheets.php
| | |____Okr.php
| | |____Application.php
| | |____Hire.php
| | |____Auth.php
| | |____FaceVerify.php
| | |____Ehr.php
| | |____Mail.php
| | |____OpticalCharRecognition.php
| | |____Doc.php
| | |____Helpdesk.php
| | |____FaceDetection.php
| | |____Im.php
| | |____Attendance.php
| | |____SpeechToText.php
| | |____Acs.php
| | |____Approval.php
| | |____Search.php
| | |____Task.php
| | |____Contact.php
| | |____Tenant.php
| | |____Pay.php
| | |____Calendar.php
| | |____Drive.php
| | |____MeetingRoom.php
| | |____Admin.php
| | |____Vc.php
| | |____Bitable.php
| | |____HumanAuthentication.php
|____Client
| |____H5InApp.php
| |____H5InBrowser.php
| |____MiniProgramInApp.php
|____Traits
| |____HasEventHandler.php
| |____HasHttpMgr.php
|____Manager.php
|____Exception.php
```

## Full picture & Our progress (66/433)

| 模块名(中文)      | 模块名(API路径)          | empty | v1    | v2    | v3   | v4   | v5   | v6   | 统计日期 |
| ----------------- | ------------------------ | ----- |-------|-------|------| ---- | ---- | ---- | -------- |
| API访问凭证       | auth                     |       |       |       | 5/5  |      |      |      | 21/09/30 |
| 身份验证(免登)    | authen                   |       | 1/4   |       |      |      |      |      | 21/09/30 |
| 通讯录            | contact                  |       |       |       | 9/27 |      |      |      | 21/09/30 |
| 消息与群组        | im                       |       | 27/31 |       |      | 1/1  |      |      | 21/09/30 |
| 日历              | calendar                 |       |       |       |      | 0/27 |      |      | 21/09/30 |
| 云文档 - 文件管理 | drive                    | 0/4   | 1/24  | 2/9   |      |      |      |      | 21/09/30 |
| 云文档 - 元数据   | suite                    | 0/2   |       |       |      |      |      |      | 21/09/30 |
| 云文档 - 文档     | doc                      |       |       | 0/5   |      |      |      |      | 21/09/30 |
| 云文档 - 电子表格 | sheets                   |       |       | 0/33  | 0/23 |      |      |      | 21/09/30 |
| 云文档 - 多维表格 | bitable                  |       | 0/21  |       |      |      |      |      | 21/09/30 |
| 会议室            | meeting_room             | 0/16  |       |       |      |      |      |      | 21/09/30 |
| 视频会议          | vc                       |       | 0/17  |       |      |      |      |      | 21/09/30 |
| 应用信息          | application              |       | 0/5   | 0/1   | 0/3  | 0/1  |      | 0/1  | 21/09/30 |
| 邮箱              | mail                     |       | 0/24  |       |      |      |      |      | 21/09/30 |
| 审批              | approval                 |       |       | 15/15 |      | 9/9  |      |      | 21/09/30 |
| 审批 - 三方审批   | approval                 |       | 0/2   | 0/2   | 0/2  |      |      |      | 21/09/30 |
| 搜索              | search                   |       |       | 0/8   |      |      |      |      | 21/09/30 |
| 服务台            | helpdesk                 |       | 0/25  |       |      |      |      |      | 21/09/30 |
| 企业信息          | tenant                   |       |       | 1/1   |      |      |      |      | 21/09/30 |
| 管理后台          | admin                    |       | 0/2   |       |      |      |      |      | 21/09/30 |
| 实名认证 - 人脸   | face_verify              |       | 0/3   |       |      |      |      |      | 21/09/30 |
| 实名认证 - 身份   | human_authentication     |       | 0/1   |       |      |      |      |      | 21/09/30 |
| AI能力 - OCR      | optical_char_recognition |       | 0/1   |       |      |      |      |      | 21/09/30 |
| AI能力 - 语音识别 | speech_to_text           |       | 0/2   |       |      |      |      |      | 21/09/30 |
| AI能力 - 机器翻译 | translation              |       | 0/2   |       |      |      |      |      | 21/09/30 |
| AI能力 - 人脸检测 | face_detection           |       | 0/1   |       |      |      |      |      | 21/09/30 |
| 打卡              | attendance               |       | 0/27  |       |      |      |      |      | 21/09/30 |
| OKR               | okr                      |       | 0/3   |       |      |      |      |      | 21/09/30 |
| 智能人事          | ehr                      |       | 2/2   |       |      |      |      |      | 21/09/30 |
| 招聘              | hire                     |       | 0/20  |       |      |      |      |      | 21/09/30 |
| 任务              | task                     |       | 0/19  |       |      |      |      |      | 21/09/30 |
| 智能门禁          | acs                      |       | 0/8   |       |      |      |      |      | 21/09/30 |


## Change log

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/:vendor/:package_name.svg?style=flat-square
[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square
[ico-travis]: https://img.shields.io/travis/:vendor/:package_name/master.svg?style=flat-square
[ico-scrutinizer]: https://img.shields.io/scrutinizer/coverage/g/:vendor/:package_name.svg?style=flat-square
[ico-code-quality]: https://img.shields.io/scrutinizer/g/:vendor/:package_name.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/:vendor/:package_name.svg?style=flat-square

[link-packagist]: https://packagist.org/packages/:vendor/:package_name
[link-travis]: https://travis-ci.org/:vendor/:package_name
[link-scrutinizer]: https://scrutinizer-ci.com/g/:vendor/:package_name/code-structure
[link-code-quality]: https://scrutinizer-ci.com/g/:vendor/:package_name
[link-downloads]: https://packagist.org/packages/:vendor/:package_name
[link-author]: https://github.com/:author_username
[link-contributors]: ../../contributors
